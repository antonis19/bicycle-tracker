FROM tiangolo/uwsgi-nginx-flask:python3.5

WORKDIR /app

ADD . /app

RUN pip3 install -r requirements.txt
