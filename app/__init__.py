from flask import Flask
from flask_restful import Resource, Api
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_socketio import SocketIO

app = Flask(__name__)
db = SQLAlchemy()
ma = Marshmallow()
socketio = SocketIO(app)

