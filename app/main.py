"""
Application
"""
from flask import Flask
from app import routes
from app.routes import *
from inspect import getmembers, ismodule
from app import app
from threading import Thread
from app.mainserver.mainServer import run
import app.database as database
import app.lib.authentication as authentication
import app.config as config


def init_routes(app):
    module_list = getmembers(routes, ismodule)
    for (name, m) in module_list:
        if name is not "glob":
            m.init(app)


def init_api(app):
    from app.routes.API import api as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')


def init_app(app):
    database.init()
    init_routes(app)
    init_api(app)
    app.secret_key = config.secret_key
    Thread(target=run).start()
    authentication.init()


init_app(app)
