import pika

mainServerHost = 'rabbitmq'
mainServerPort = 5672

# In this configuration all the regional servers run on the same host
regionalServerHost = '0.0.0.0'
regionalServerPort = 10000

ipForwardingHost = '0.0.0.0'
ipForwardingPort = 9998

regionalServers = []
regionalServers.append(
    {"ip": regionalServerHost, "port": regionalServerPort, "longitude": '6.535993', "latitude": '53.240764',
     "name": "Groningen bernoulliborg"})

# Add regional servers according to your own liking, but make sure
# that they are running

# regionalServers.append(
#     {"ip": regionalServerHost, "port": regionalServerPort, "longitude": '6.566502', "latitude": '53.219383',
#      "name": "Groningen Center"})
# regionalServers.append(
#     {"ip": regionalServerHost, "port": regionalServerPort + 2, "longitude": '4.895168', "latitude": '52.370216',
#      "name": "Groningen West"})
# regionalServers.append(
#     {"ip": regionalServerHost, "port": regionalServerPort + 3, "longitude": '4.473991', "latitude": '51.921283',
#      "name": "Groningen East"})
