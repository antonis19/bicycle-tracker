#!/usr/bin/env python
from app.mainserver.config import *
from app import socketio
import pika
import json
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

bikes = {}  # the bike locations to be displayed as a live map


def start_mq():
    host = mainServerHost
    port = mainServerPort
    try:
        logger.info('Setting up MQ receiver on %s:%s' % (host, port))
        credentials = pika.PlainCredentials("admin", "admin")
        connection = pika.BlockingConnection(pika.ConnectionParameters(host, port, credentials=credentials))
        channel = connection.channel()
        channel.queue_declare(queue='locations')
        start_consuming(channel)
    except:
        logger.error('Error: Could not start MQ receiver')


def callback(ch, method, properties, body):
    logger.info(" [x] Received %r\n" % body)
    dataParse = json.loads(body.decode())
    try:
        bike_id = dataParse.get("bikeId")
        latitude = dataParse.get("latitude")
        longitude = dataParse.get("longitude")
        timestamp = dataParse.get("timestamp")
        bikes[bike_id] = {"latitude": latitude, "longitude": longitude, "timestamp": timestamp, "id": bike_id}
        socketio.emit('new loc', bikes[bike_id])
    except:
        # This should never happen because regional server will not allow it
        logger.error("[ERROR] Expected json but got: %s" % dataParse)


def start_consuming(channel):
    channel.basic_consume(callback, queue='locations', no_ack=True)
    logger.info('[*] Waiting for messages')
    channel.start_consuming()
