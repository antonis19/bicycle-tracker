import socket
import json
import math
from app.mainserver.config import *
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


# start listening for connections from clients
def start_listening():
    host = ipForwardingHost
    port = ipForwardingPort
    listening_socket = socket.socket(socket.AF_INET,
                                    socket.SOCK_STREAM)  # create a socket object
    listening_socket.bind(
        (host, port))  # associate the socket with the regionalServers address
    listening_socket.listen(5)
    while True:
        logger.info('Waiting for connection on %s:%s' % (host, port))
        clientsocket, addr = listening_socket.accept()
        logger.info("Got a connection from client %s" % str(addr))
        data = clientsocket.recv(1024)
        logger.info("got:" + str(data.decode()))
        dataParse = json.loads(data.decode())
        try:
            latitude = dataParse.get("latitude")
            longitude = dataParse.get("longitude")
        except:
            logger.warning('[Error] JSON does not contain required variable')
        closestIP, closestPort = calculate_nearest_regional_server(regionalServers, latitude, longitude)
        data = json.dumps({'ip': closestIP, 'port': closestPort})
        clientsocket.send(data.encode())
        clientsocket.close()


def get_euclidean_distance(x1, y1, x2, y2):
    return math.hypot(float(x2) - float(x1), float(y2) - float(y1))


def calculate_nearest_regional_server(regional_servers, latitude, longitude):
    import sys
    last_distance = sys.maxsize
    pos = 0
    for i in range(0, len(regional_servers)):
        dist = get_euclidean_distance(latitude, longitude, regional_servers[i].get("latitude"),
                                      regional_servers[i].get("longitude"))
        if dist < last_distance and regional_server_is_running(regional_servers[i].get("ip"),
                                                               regional_servers[i].get("port")):
            lastDistance, pos = dist, i
    logger.info(
        "closest regionalServer FOUND is: %s %s\n" % (
        regional_servers[pos].get("ip"), regional_servers[pos].get("name")))
    return regional_servers[pos].get("ip"), regional_servers[pos].get("port")


def regional_server_is_running(ip, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    if s.connect_ex((ip, port)):
        ret = False
    else:
        ret = True
    s.close()
    return ret
