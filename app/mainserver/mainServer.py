import threading
from app.mainserver import start_listening, start_mq
from time import sleep
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


def run():
    sleep(15)
    threading.Thread(target=start_mq).start()
    threading.Thread(target=start_listening).start()
