"""
An example UnitTest
"""
import unittest


class TestExample(unittest.TestCase):
    """
    Provides an example test class. Use this as a template for new tests.
    To run all tests, run: npm test.
    """

    def test_square(self):
        """
        Asserts whether 3^2 equals 9.
        :return:
        """
        self.assertEqual(3 ** 2, 9)


if __name__ == '__main__':
    unittest.main()
