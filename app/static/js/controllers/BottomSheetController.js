app.controller('BottomSheetController', ['$scope', '$mdBottomSheet', function ($scope, $mdBottomSheet) {
	$scope.items = [
		{name: 'Back', icon: 'chevron-left'},
		{name: 'Delete', icon: 'trash'},
		{name: 'Edit', icon: 'pencil'}
	];

	$scope.listItemClick = function ($index) {
		var clickedItem = $scope.items[$index];
		$mdBottomSheet.hide(clickedItem);
	};
}]);

app.run(function ($templateRequest) {
	var prefix = "/static/dependencies/open-iconic/svg/";
	var urls = [
		'chevron-left',
		'trash',
		'pencil'
	];

	angular.forEach(urls, function (url) {
		$templateRequest(prefix + url + ".svg");
	});

});

