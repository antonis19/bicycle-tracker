app.controller('SheetController', ['$scope', '$timeout', '$mdBottomSheet', '$mdToast',
	function ($scope, $timeout, $mdBottomSheet, $mdToast) {
		$scope.alert = '';

		$scope.showGridBottomSheet = function () {
			$scope.alert = '';
			$mdBottomSheet.show({
				templateUrl: '/static/js/directives/html/bottom-sheet.html',
				controller: 'BottomSheetController'
			}).then(function (clickedItem) {
				$mdToast.show(
					$mdToast.simple()
						.textContent(clickedItem['name'] + ' clicked!')
						.position('bottom left')
						.hideDelay(4500)
				);
			}).catch(function (error) {
				// User clicked outside or hit escape
			});
		};
	}]);
