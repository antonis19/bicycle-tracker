app.controller('AddBicycleController', ['$scope', '$http', function($scope, $http) {

	$scope.editBicycle = function() {
		var bike = {
			bicycle: $('#bicycle').val()
		};
		$http.patch('/api/bike/' + $('#bikeId').val() + '/', bike)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/bike/'+ $('#bikeId').val() + '/view';
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});
	};

	$scope.addBicycle = function() {
		var bike = {
			bicycle: $('#bicycle').val(),
			ownerId: $('#ownerId').val()
		};
		$http.post('/api/bike/', bike)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/bicycle/view';
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});
	};

}]);
