app.controller('BicycleController', ['$scope', '$http', '$mdDialog', function ($scope, $http, $mdDialog) {
	$scope.setBikes = function () {
		$http.get('/api/bike')
			.then(function (data) {
				$scope.bikes = data.data;
			}, function (err) {
				console.log(err);
			});

		$http.get('/api/bike/ownerId=' + $('#ownerId').val())
			.then(function (data) {
				$scope.myBikes = data.data;
			}, function (err) {
				console.log(err);
			});
	};

	$scope.setBikes();


	$scope.showConfirm = function (ev) {
		// Appending dialog to document.body to cover sidenav in docs app
		var confirm = $mdDialog.confirm()
			.title('Would you like to delete your bicycle?')
			.textContent('You are deleting the bicycle: "' + $(ev.currentTarget).siblings('.text-warning').text() +
				'".  Once you have deleted it, there is no turning back.')
			.ariaLabel('Delete bicycle')
			.targetEvent(ev)
			.ok('Delete my bicycle')
			.cancel('Cancel');

		$mdDialog.show(confirm).then(function () {
			$http.delete('/api/bike/' + $(ev.currentTarget).data('id') + '/')
				.then(function (data) {
					$scope.setBikes();
				}, function (err) {
					console.log(err);
				});
		}, function () {
			// Do nothing
		});
	};
}]);
