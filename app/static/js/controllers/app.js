var app = angular.module('BicycleTracker', ['ngMaterial', 'ngMessages'])
	.config(function ($mdIconProvider, $mdThemingProvider) {
		$mdThemingProvider.theme('default')
			.dark();
		var prefix = "/static/dependencies/open-iconic/svg/";
		$mdIconProvider
			.icon('chevron-left', prefix + 'chevron-left.svg', 24)
			.icon('trash', prefix + 'trash.svg', 24)
			.icon('pencil', prefix + 'pencil.svg', 24);
	});
