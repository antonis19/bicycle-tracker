app.controller('SignupController', ['$scope', '$http', function ($scope, $http) {
	$scope.username = "";
	$scope.password = "";
	$scope.hasError = false;

	$scope.signup = function () {
		$scope.username = $('#username').val();
		$scope.password = $('#password').val();
		var account = {
			username: $scope.username,
			password: $scope.password
		};
		$http.post('/api/account/', account)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/login';
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});
	};

	$http.get('/api/account')
		.then(function (data) {
			$scope.accounts = data.data;
		}, function (err) {
			console.log(err);
		});

}]);
