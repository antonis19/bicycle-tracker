app.controller('TheftController', ['$scope', '$http', '$mdDialog', function ($scope, $http, $mdDialog) {
	$scope.setTheft = function () {
		$http.get('/api/theftreport')
			.then(function (data) {
				$scope.theft = data.data;
			}, function (err) {
				console.log(err);
			});

		$http.get('/api/theftreport/ownerId=' + $('#ownerId').val())
			.then(function (data) {
				$scope.myTheft = data.data;
			}, function (err) {
				console.log(err);
			});
	};

	$scope.setTheft();

	$scope.showConfirm = function (ev) {
		// Appending dialog to document.body to cover sidenav in docs app
		var confirm = $mdDialog.confirm()
			.title('Would you like to delete your theft report?')
			.textContent('You are deleting the theft report for: "' + $(ev.currentTarget).parent().parent().find('.name').text() +
				'".  Once you have deleted it, there is no turning back.')
			.ariaLabel('Delete theft report')
			.targetEvent(ev)
			.ok('Delete the theft report')
			.cancel('Cancel');

		$mdDialog.show(confirm).then(function () {
			$http.delete('/api/theftreport/' + $(ev.currentTarget).data('id') + '/')
				.then(function (data) {
					$scope.setTheft();
				}, function (err) {
					console.log(err);
				});
		}, function () {
			// Do nothing
		});
	};

}]);
