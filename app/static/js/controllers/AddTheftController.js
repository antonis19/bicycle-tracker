app.controller('AddTheftController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
	$scope.maxDate = new Date();
	//$scope.date = new Date();
	$scope.coordinateError = false;

	$scope.editTheft = function () {
		var coordinate = $('#map').data('coordinate');
		if (!coordinate) {
			$scope.coordinateError = true;
			return;
		}

		var theft = {
			latitude: coordinate.lat(),
			longitude: coordinate.lng()
		};
		$http.patch('/api/theftreport/' + $('#theftReportId').val() + '/', theft)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/theft/'+ $('#theftReportId').val() + '/view';
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});

	};

	$scope.addTheft = function () {
		var coordinate = $('#map').data('coordinate');
		if (!coordinate) {
			$scope.coordinateError = true;
			return;
		}
		var theft = {
			bikeId: $rootScope.bike.id,
			latitude: coordinate.lat(),
			longitude: coordinate.lng(),
			timestamp: $scope.date.toString()
		};
		$http.post('/api/theftreport/', theft)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/theft/view';
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});
	};

	$http.get('/api/bike/ownerId=' + $('#ownerId').val())
		.then(function (data) {
			$scope.bikes = data.data;
			if ($scope.bikes.length > 0)
				$rootScope.bike = $scope.bikes[0];
		}, function (err) {
			console.log(err);
		});
}]);
