app.controller('LoginController', ['$scope', '$http', function ($scope, $http) {
	$scope.username = "";
	$scope.password = "";
	$scope.hasError = false;

	$scope.login = function () {
		$scope.username = $('#username').val();
		var account = {
			username: $scope.username,
			password: $scope.password
		};
		$http.post('/login', account)
			.then(function (data) {
				if (data.status === 200) {
					window.location = '/overview'
				} else {
					$scope.hasError = true;
				}
			}, function (err) {
				$scope.hasError = true;
			});
	};

	$http.get('/api/account')
		.then(function (data) {
			$scope.accounts = data.data;
		}, function (err) {
			console.log(err);
		});

}]);


