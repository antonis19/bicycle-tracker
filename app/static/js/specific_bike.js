var socket = io.connect('http://' + document.domain + ':' + location.port);

var map;
var markers = [];

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 12,
		center: loc,
		mapTypeId: 'terrain'
	});

	if (loc.lat >  0 && loc.lng > 0) {
		addMarker(loc);
	}

	socket.on('new loc', function (locData) {
		if (locData.id === id) {
			console.log(locData);
			var coord = {lat: locData.latitude, lng: locData.longitude};
			addMarker(coord);
		}
	});
}

function addMarker(location) {
	var marker = new google.maps.Marker({
		position: location,
		map: map
	});
	deleteMarkers();
	markers.push(marker);
}

function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

function clearMarkers() {
	setMapOnAll(null);
}

function showMarkers() {
	setMapOnAll(map);
}

function deleteMarkers() {
	clearMarkers();
	markers = [];
}
