var map;
var markers = [];

function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 12,
		center: {lat: 53.2167574, lng: 6.5624566},
		mapTypeId: 'terrain'
	});

	map.addListener('click', function (event) {
		addMarker(event.latLng);
	});
}

function addMarker(location) {
	var marker = new google.maps.Marker({
		position: location,
		map: map
	});
	deleteMarkers();
	$('#map').data('coordinate', marker.position);
	markers.push(marker);
}

function setMapOnAll(map) {
	for (var i = 0; i < markers.length; i++) {
		markers[i].setMap(map);
	}
}

function clearMarkers() {
	setMapOnAll(null);
}

function showMarkers() {
	setMapOnAll(map);
}

function deleteMarkers() {
	clearMarkers();
	markers = [];
}
