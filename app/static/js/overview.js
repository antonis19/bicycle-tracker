var socket = io.connect('http://' + document.domain + ':' + location.port);
var map, heatmap, trafficMap, trafficHeatmap;

function initMap() {
	map = new google.maps.Map(document.getElementById('theftMap'), {
		zoom: 13,
		center: {lat: 53.2167574, lng: 6.5624566},
		mapTypeId: 'satellite'
	});

	$.get("/api/theftreport", function (data) {
		var points = [];
		for (var i = 0; i < data.length; ++i) {
			points[i] = new google.maps.LatLng(data[i].latitude, data[i].longitude)
		}
		heatmap = new google.maps.visualization.HeatmapLayer({
			data: points,
			map: map
		});

		socket.on('new theft', function (data) {
			var coord = new google.maps.LatLng(data.latitude, data.longitude);
			heatmap.data.push(coord);
		});
	});


	trafficMap = new google.maps.Map(document.getElementById('densityMap'), {
		zoom: 13,
		center: {lat: 53.2167574, lng: 6.5624566},
		mapTypeId: 'satellite'
	});

	$.get("/api/bike", function (data) {
		var points = [];
		/*for (var i = 0; i < data.length; ++i) {
			points[i] = new google.maps.LatLng(data[i].latitude, data[i].longitude)
		}*/
		trafficHeatmap = new google.maps.visualization.HeatmapLayer({
			data: points,
			map: trafficMap
		});
		socket.on('new loc', function (locData) {
			console.log(locData);
			var coord = new google.maps.LatLng(locData.latitude, locData.longitude);
			trafficHeatmap.data.setAt(locData.id, coord);
		});
	});

}

function toggleHeatmap() {
	heatmap.setMap(heatmap.getMap() ? null : map);
}

function changeGradient() {
	var gradient = [
		'rgba(0, 255, 255, 0)',
		'rgba(0, 255, 255, 1)',
		'rgba(0, 191, 255, 1)',
		'rgba(0, 127, 255, 1)',
		'rgba(0, 63, 255, 1)',
		'rgba(0, 0, 255, 1)',
		'rgba(0, 0, 223, 1)',
		'rgba(0, 0, 191, 1)',
		'rgba(0, 0, 159, 1)',
		'rgba(0, 0, 127, 1)',
		'rgba(63, 0, 91, 1)',
		'rgba(127, 0, 63, 1)',
		'rgba(191, 0, 31, 1)',
		'rgba(255, 0, 0, 1)'
	];
	heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
	heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
	heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

