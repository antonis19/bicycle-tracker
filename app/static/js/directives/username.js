app.directive('username', function () {
	return {
		require: 'ngModel',
		scope: {
			'accounts': '=',
			'mul': '='
		},
		link: function (scope, elm, attrs, ctrl) {
			ctrl.$validators.username = function (modelValue, viewValue) {
				if (ctrl.$isEmpty(modelValue)) {
					return true;
				}

				var res = scope.accounts.some(function(obj) {
					return obj.username === modelValue;
				});
				return scope.mul ? !res : res;
			};
		}
	};
});
