#!/usr/bin/env python
import pika
import config
import logging

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)

port = config.mainServerPort


def create_channel(host):
    credentials = pika.PlainCredentials(config.pikaUsername, config.pikaPassword)
    parameters = pika.ConnectionParameters(host, port, '/', credentials)
    if host == 'localhost':
        parameters = pika.ConnectionParameters(host=host)
    try:
        connection = pika.BlockingConnection(parameters)
    except:
        logger.warning('[ERROR] MQ connection: Ensure service rabbitmq-server has started on %s:%s\n' % (host, port))
        return None
    channel = connection.channel()
    channel.queue_declare(queue='locations')
    return channel


def send(channel, body):
    if channel is not None and channel.is_open:
        channel.basic_publish(exchange='', routing_key='locations', body=body)
    else:
        logger.info("[ERROR] Channel blocked. If error persists restart server")
