import socket
import threading
import time
import json
import sender
import config
import logging

time.sleep(18)

mainServerHost = config.mainServerHost

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class ClientThreadHandler(threading.Thread):
    # class constructor called by the mainThread
    def __init__(self, conn, addr, port):
        threading.Thread.__init__(self)
        self.socket = conn
        self.addr = addr

    def run(self):
        thread_id = threading.current_thread().getName()
        logger.info("[%s] New thread started for %s" % (thread_id, self.addr))
        while True:
            data = self.socket.recv(1024)
            if not data:
                self.socket.close()
                break
            logger.info("[%s] RegionalServer got: %s" % (thread_id, data.decode()))
            if check_integrity(data.decode()):
                channel = sender.create_channel(mainServerHost)
                sender.send(channel, data)


def check_integrity(data):
    if 'bikeId' in data and 'longitude' in data and 'latitude' in data and 'timestamp' in data:
        return True
    return False


def startListening(host, port):
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # create a socket object
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # reuse port
    serversocket.bind((host, port))  # associate the socket with the server address
    serversocket.listen(5)
    while True:
        logger.info("\nWaiting for a connection on %s:%s" % (host, port))
        conn, addr = serversocket.accept()
        logger.info("Got a connection from client %s" % str(addr))
        ClientThreadHandler(conn, addr, port).start()


def main():
    host = config.regionalServerHost
    port = config.regionalServerPort  # NOTE: change port if no data is being  received
    startListening(host, port)


if __name__ == "__main__":
    main()
