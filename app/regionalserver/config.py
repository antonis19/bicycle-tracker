# get the current ip, if connected to the internet ping 8.8.8.8
def get_ip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    if s.connect_ex(("8.8.8.8", 80)):
        ip = socket.gethostbyname(socket.gethostname())
    else:
        ip = s.getsockname()[0]
    s.close()
    return ip


mainServerHost = 'rabbitmq'
mainServerPort = 5672  # receiver

regionalServerHost = '0.0.0.0'
regionalServerPort = 10000

pikaUsername = 'admin'
pikaPassword = 'admin'
