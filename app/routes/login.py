from flask import request, render_template, redirect, url_for
from app.lib import url_handler
from app.models.Account import Account
from app.forms.LoginForm import LoginForm
from flask_login import login_user, current_user


def init(app):
    @app.route('/login', methods=['GET', 'POST'])
    def login():
        next = url_handler.get_redirect_target()
        form = LoginForm(request.form)
        if request.method == 'POST':
            if current_user and current_user.is_authenticated:
                return "Already logged in", 200
            details = request.get_json()
            account = Account.query.filter_by(username=details['username']).first()
            if account and details['password'] == account.password:
                login_user(account)
                return "Success", 200
            return "Invalid credentials", 401

        else:
            if current_user and current_user.is_authenticated:
                return redirect(url_for('overview'))
            return render_template('login.html', no_menu=True, next=next, form=form)
