from flask import render_template, request
from app.forms.BicycleForm import BicycleForm
from flask_login import current_user, login_required
from app.models.Bike import Bike


def init(app):
    @app.route('/bike/<int:id>/edit')
    @login_required
    def edit_bicycle(id):
        bike = Bike.query.get(id)
        if bike is None or bike.account_id is not current_user.id:
            return "This bike does not belong to the current user", 400
        return render_template('bicycle/edit.html', user_id=current_user.id, bike=bike, form=BicycleForm(request.form))
