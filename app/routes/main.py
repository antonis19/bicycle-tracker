from flask import redirect, url_for


def init(app):
    @app.route('/')
    def main_route():
        return redirect(url_for('login'))
