from flask import render_template, request
from app.forms.BicycleForm import BicycleForm
from flask_login import current_user, login_required


def init(app):
    @app.route('/theft/add')
    @login_required
    def theft_bicycle():
        return render_template('theft/add.html', form=BicycleForm(request.form), user_id=current_user.id)
