from flask import render_template, request
from flask_login import current_user, login_required
from app.models.TheftReport import TheftReport


def init(app):
    @app.route('/theft/<int:id>/view')
    @login_required
    def specific_theft(id):
        theft = TheftReport.query.get(id)
        if theft is None or theft.bike.account_id is not current_user.id:
            return "This theft report does not belong to the current user", 400
        return render_template('theft/specific.html', user_id=current_user.id, theft=theft)
