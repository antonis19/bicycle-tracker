from flask import render_template, request
from app.forms.SignupForm import SignupForm
from app.models.Account import Account


def init(app):
    @app.route('/signup')
    def signup():
        form = SignupForm(request.form)
        return render_template('signup.html', no_menu=True, form=form)
