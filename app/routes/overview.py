from flask import render_template
from flask_login import login_required


def init(app):
    @app.route('/overview')
    @login_required
    def overview():
        return render_template('overview.html')
