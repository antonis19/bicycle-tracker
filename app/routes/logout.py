from flask import redirect, url_for
from flask_login import logout_user
from flask_login import login_required


def init(app):
    @app.route('/logout')
    @login_required
    def logout():
        logout_user()
        return redirect(url_for('login'))
