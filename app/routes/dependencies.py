import os, inspect
from flask import send_from_directory, abort

public_dependencies = ('angular', 'bootstrap', 'jquery', 'popper.js', 'open-iconic', 'angular-material-source')

# This route is useful when you use npm for front-end dependencies.
# This route is currently disabled, as we have provided the front-end dependencies our selves.
# Which would allow for a more easy installation on your side.


def init(app):
    @app.route('/static/dependencies/<path:path>')
    def dependencies(path):
        p = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
        p = p + '/../../node_modules'
        if path.startswith(public_dependencies):
            return send_from_directory(p, path)
        else:
            abort(404)

