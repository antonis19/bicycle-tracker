from os.path import dirname, basename, isfile
import glob

modules = glob.glob(dirname(__file__) + "/*.py")
__all__ = [basename(f)[:-3] for f in modules if isfile(f) and not f.endswith('__init__.py')]

from flask import Blueprint, jsonify

api = Blueprint('api', __name__)

from . import accountController, bikeController, theftReportController


def init(app):
    return None
