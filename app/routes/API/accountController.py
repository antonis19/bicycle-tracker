from flask import request
from app.models import Account
from app import db
from . import api
from .utils import *


@api.route('/account/', methods=['GET'])
def get_accounts():
    return get_all(Account)


@api.route('/account/<int:id>/', methods=['GET'])
def get_account(id):
    return json_response(Account.query.get(id), 200)


@api.route('/account/<string:username>/', methods=['GET'])
def get_account_by_username(username):
    return json_response(Account.query.filter_by(username=username).first(), 200)


@api.route('/account/', methods=['POST'])
def create_account():
    return add(request, Account, ["username", "password"])
