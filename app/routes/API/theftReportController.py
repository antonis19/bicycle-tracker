from flask import request, jsonify
from app.models import TheftReport
from app import db, socketio
from . import api
from .utils import *
from dateutil import parser
from flask_login import login_required


# Get all theft reports
@api.route('/theftreport/', methods=['GET'])
@login_required
def get_all_theft_reports():
    return get_all(TheftReport)


# Add a theft report
@api.route('/theftreport/', methods=['POST'])
@login_required
def add_theft_report():
    arg_list = validate_json(request, ["bikeId", "latitude", "longitude", "timestamp"])
    theft_report = TheftReport(*arg_list)
    db.session.add(theft_report)
    db.session.commit()
    socketio.emit("new theft", theft_report.to_json())
    return message_response('Theft report added successfully', 200)


# Get a theft report by its id
@api.route('/theftreport/<int:id>/', methods=['GET'])
@login_required
def get_theft_report(id):
    return json_response(TheftReport.query.get(id), 200)


# Delete a theft report given its id
@api.route('/theftreport/<int:id>/', methods=['DELETE'])
@login_required
def delete_theft_report(id):
    return delete(TheftReport, id)


@api.route('/theftreport/ownerId=<int:id>/', methods=['GET'])
@login_required
def get_theft_report_by_owner(id):
    theft_reports = TheftReport.query.filter(TheftReport.bike.has(account_id=id))
    theft_reports_list = [theftReport.to_json() for theftReport in theft_reports]
    return jsonify(theft_reports_list), 200


# Update theft report
@api.route('/theftreport/<int:id>/', methods=['PATCH'])
@login_required
def update_theft_report(id):
    theft_report = TheftReport.query.get(id)
    if theft_report is None:
        return message_response('Theft report not found', 404)

    try:
        [latitude, longitude] = validate_json(request, ["latitude", "longitude"])

        theft_report.latitude = latitude
        theft_report.longitude = longitude
        db.session.commit()
        return message_response('Theft report info updated successfully', 200)
    except Exception as e:
        return message_response(repr(e), 400)
