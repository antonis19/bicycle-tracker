from flask import request, jsonify
from app.models import Bike
from app import db
from . import api
from .utils import *
from flask_login import login_required


# Get all bikes
@api.route('/bike/', methods=['GET'])
@login_required
def get_bikes():
    return get_all(Bike)


# Get all bikes with a specific ownerId
@api.route('/bike/ownerId=<int:id>/', methods=['GET'])
@login_required
def get_bikes_by_owner(id):
    return get_all(Bike, account_id=id)


# Add a bike
@api.route('/bike/', methods=['POST'])
@login_required
def add_bike():
    return add(request, Bike, ["ownerId", "bicycle"])


# Get a bike by its id
@api.route('/bike/<int:id>/', methods=['GET'])
@login_required
def get_bike(id):
    return json_response(Bike.query.get(id), 200)


# Delete a bike given its id
@api.route('/bike/<int:id>/', methods=['DELETE'])
@login_required
def delete_bike(id):
    return delete(Bike, id)


# Update bike information
@api.route('/bike/<int:id>/', methods=['PATCH'])
def update_bike(id):
    bike = Bike.query.get(id)
    if bike is None:
        return message_response('Bike not found', 404)

    try:
        [latitude, longitude, timestamp] = validate_json(request, ["latitude", "longitude", "timestamp"])
        bike.latitude = latitude
        bike.longitude = longitude
        bike.timestamp = timestamp
    except:
        try:
            [name] = validate_json(request, ["bicycle"])
            bike.name = name
        except Exception as e:
            return message_response(repr(e), 400)

    db.session.commit()
    return message_response('Bike info updated successfully', 200)
