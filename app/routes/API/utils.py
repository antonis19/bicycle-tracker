from flask import jsonify
from app import db
from dateutil import parser


def message_response(message, status_code):
    return jsonify({"message": message}), status_code


def json_response(body, status_code):
    if body is None:
        return message_response("Entity not found", 404)
    return jsonify(body.to_json()), status_code


def get_all(entity, **kwargs):
    objs = entity.query.all()
    for key in kwargs:
        filter_input = {key: kwargs[key]}
        objs = entity.query.filter_by(**filter_input)
    obj_list = [obj.to_json() for obj in objs]
    return jsonify(obj_list), 200


def delete(entity, id):
    obj = entity.query.get(id)
    if obj is None:
        return message_response('Entity not found', 404)
    db.session.delete(obj)
    db.session.commit()
    return message_response('Entity deleted successfully', 200)


def validate_json(request, parameters):
    json = request.get_json()
    if json is None:
        raise Exception("body is empty")

    arg_list = []
    for parameter in parameters:
        value = json.get(parameter, None)
        if value is None:
            raise Exception(parameter + ' is empty')
        if parameter is "timestamp":
            value = parser.parse(value)
        arg_list.append(value)

    return arg_list


def add(request, entity, parameters):
    try:
        arg_list = validate_json(request, parameters)
        obj = entity(*arg_list)
        db.session.add(obj)
        db.session.commit()
        return message_response('Entity added succesfully', 200)
    except Exception as e:
        return message_response(repr(e), 400)
