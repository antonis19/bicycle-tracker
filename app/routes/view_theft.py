from flask import render_template, request
from app.forms.BicycleForm import BicycleForm
from flask_login import current_user
from flask_login import login_required


def init(app):
    @app.route('/theft/view')
    @login_required
    def theft_view():
        return render_template('theft/view.html', form=BicycleForm(request.form), user_id=current_user.id)
