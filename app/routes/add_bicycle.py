from flask import render_template, request
from app.forms.BicycleForm import BicycleForm
from flask_login import current_user, login_required


def init(app):
    @app.route('/bicycle/add')
    @login_required
    def add_bicycle():
        return render_template('bicycle/add.html', form=BicycleForm(request.form), ownerId=current_user.id)
