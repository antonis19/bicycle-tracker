from flask import render_template, request
from flask_login import current_user, login_required
from app.models.Bike import Bike


def init(app):
    @app.route('/bike/<int:id>/view')
    @login_required
    def specific_bicycle(id):
        bike = Bike.query.get(id)
        if bike is None or bike.account_id is not current_user.id:
            return "This bike does not belong to the current user", 400
        return render_template('bicycle/specific.html', user_id=current_user.id, bike=bike)
