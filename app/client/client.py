import threading
import socketClient
import apiClient
import sys


def run(bikeId):
    threading.Thread(target=apiClient.startRunning, args=(bikeId,)).start()
    threading.Thread(target=socketClient.startRunning, args=(bikeId,)).start()

def main() :
    if(len(sys.argv)==1):
        run(1)
    else:
        run(sys.argv[1]) 

if __name__ == '__main__':
    main()
