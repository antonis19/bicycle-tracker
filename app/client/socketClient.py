# client.py  
import socket
import sys
import datetime
import json
import random
import config
import apiClient
import threading
import time
import logging
from getCurrentLocation import getCurrentLocation

latitude = config.latitude
longitude = config.longitude

bikeId = -1

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)


def sendCurrentLocation(host, port):
    global latitude, longitude
    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        clientsocket.connect((host, port))
    except:
       raise Exception('Could not connect to host = %s port %s\n' % (host, port))
    latitude, longitude, currentTime = getCurrentLocation(latitude, longitude)
    data = json.dumps({"bikeId": bikeId, "longitude": longitude, "latitude": latitude, "timestamp": currentTime})
    try:
        clientsocket.send(data.encode())
    except:
        raise  Exception('Could not send location')
    return clientsocket


def getClosestRegionalServer(mainServerHost, mainServerPort):
    logger.info("[Client %s] Sending location to ipForwarding to obtain regional server ip" % bikeId)
    while True:
        try:
            clientsocket = sendCurrentLocation(mainServerHost, mainServerPort)
            break
        except:
            logger.warning('Could not connect to main server, reconnecting...')
            time.sleep(2)
    data = clientsocket.recv(1024)
    logger.info("[Client %s] Received from IpForwarder: Regional Server= %s" % (bikeId, data))
    clientsocket.close()
    dataParse = json.loads(data.decode())
    regionalHost = dataParse.get("ip", None)
    regionalPort = dataParse.get("port", None)
    return regionalHost, regionalPort


def startRunning(id):
    global bikeId
    bikeId = id
    mainServerHost = config.ipForwardingHost
    mainServerPort = config.ipForwardingPort
    regionalServerHost, regionalServerPort = getClosestRegionalServer(mainServerHost, mainServerPort)
    while  regionalServerHost is None or regionalServerPort is None:
        logger.warning('Could not connect to regional server, obtaining new regional server...')
        regionalServerHost, regionalServerPort = getClosestRegionalServer(mainServerHost, mainServerPort)
        time.sleep(10)
    while True:
        try:
            logger.info("[#%s Sending] Location and timestamp to regional server %s:%s\n" % (bikeId, 
            regionalServerHost, regionalServerPort))
            sendCurrentLocation(regionalServerHost, regionalServerPort).close()
            time.sleep(config.socketClientSleepTime)
        except:
            # reconnect
            logger.warning('Something went wrong sending through socket. Trying to reconnect')
            regionalServerHost, regionalServerPort = getClosestRegionalServer(mainServerHost, mainServerPort)


def main():
    startRunning(bikeId)


if __name__ == "__main__":
    main()
