import requests
from  getCurrentLocation import getCurrentLocation
import time
import threading
import config
import random
import logging

apiPath = config.apiurl
timeout = config.timeout
apiClientSleepTime = config.apiClientSleepTime

logging.basicConfig(level = logging.INFO)
logger = logging.getLogger(__name__)


def startRunning(bikeId):
    latitude = config.latitude
    longitude = config.longitude
    patchPath = apiPath + str(bikeId) + '/'  # the path to send HTTP requests to
    while True:
        latitude, longitude, timestamp = getCurrentLocation(latitude, longitude)
        body = {'latitude': latitude, 'longitude': longitude, 'timestamp': timestamp}
        try:
            resp = requests.patch(patchPath, json=body, timeout=timeout)
            logger.info('status code %s' % resp.status_code)
            logger.info(resp.text)
        except:
            logger.warning('[Error] Could not make http request')
        time.sleep(apiClientSleepTime)  # sleep before updating again


def main():
    bikeId = 1
    threading.Thread(target=startRunning(bikeId)).start()


if __name__ == '__main__':
    main()
