import random

#Set this yourself with accord to your main server's public ip
#if ports are blocked use the machine's local ip e.g 192.168.0.5
mainServerHost = '172.18.0.1'

ipForwardingHost = mainServerHost
ipForwardingPort = 9998

longitude = random.uniform(6.55, 6.6)
latitude = random.uniform(53.207, 53.24)

apiPort = 5000

timeout = 4 # seconds
apiClientSleepTime = 30 # seconds
socketClientSleepTime = 3 # seconds


apiurl = 'http://'+mainServerHost+':' + str(apiPort) + '/api/bike/'
