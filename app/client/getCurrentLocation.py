import random
import datetime


def getCurrentLocation(latitude, longitude):
    latitude += random.uniform(-0.0001, 0.0005)
    longitude += random.uniform(-0.0001, 0.0001)
    timestamp = str(datetime.datetime.now())
    return latitude, longitude, timestamp
