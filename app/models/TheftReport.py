"""
Represents the Theft Report model.
"""
from sqlalchemy import Column, DateTime, Integer, Float, ForeignKey
from app import db
from sqlalchemy.orm import relationship


class TheftReport(db.Model):
    """
    A theft report for a bicycle
    """
    __tablename__ = 'theft_report'
    id = Column(Integer, primary_key=True)
    bike_id = Column(Integer, ForeignKey('bike.id'))
    latitude = Column(Float(precision=10))
    longitude = Column(Float(precision=10))
    timestamp = Column(DateTime)

    bike = relationship("Bike", back_populates="theft_reports")

    def __init__(self, bike_id, latitude, longitude, timestamp):
        self.bike_id = bike_id
        self.latitude = latitude
        self.longitude = longitude
        self.timestamp = timestamp

    def to_json(self):
        body = {
            'id': self.id,
            'bikeId': self.bike_id,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'timestamp': self.timestamp.isoformat(),
            'name': self.bike.name
        }
        return body

    def __repr__(self):
        return '<TheftReport> {0}: {1} : {2} : {3} '.format(self.bike_id, self.latitude, self.longitude, self.timestamp)
