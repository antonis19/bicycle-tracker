"""
Represents the model for an account.
"""
from app import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from app.models.Bike import Bike


class Account(db.Model):
    __tablename__ = 'account'
    id = Column(Integer, primary_key=True)
    username = Column(String(100), unique=True)
    password = Column(String(100))
    is_authenticated = False
    bikes = relationship("Bike", order_by=Bike.id, back_populates="account")

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.is_authenticated = False

    def to_json(self):
        body = {
            'id': self.id,
            'username': self.username
        }
        return body

    def is_active(self):
        return True

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<Account {0}>'.format(self.username)
