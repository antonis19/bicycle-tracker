"""
Represents the Coordinate model.
"""
from sqlalchemy import Column, DateTime, Integer, Float, ForeignKey, String
from app import db
from sqlalchemy.orm import relationship
from app.models.TheftReport import TheftReport
from datetime import datetime


class Bike(db.Model):
    """
    A bike belonging to an Account , and whose last location is tracked periodically.
    """
    __tablename__ = 'bike'
    id = Column(Integer, primary_key=True)
    account_id = Column(Integer, ForeignKey('account.id'))
    name = Column(String())
    # last seen location
    latitude = Column(Float(precision=10))
    longitude = Column(Float(precision=10))
    timestamp = Column(DateTime)

    account = relationship("Account", back_populates="bikes")
    theft_reports = relationship("TheftReport", order_by=TheftReport.id, back_populates="bike")

    def __init__(self, account_id, name):
        self.account_id = account_id
        self.latitude = 0
        self.longitude = 0
        self.timestamp = datetime.now()
        self.name = name

    def to_json(self):
        body = {
            'id': self.id,
            'ownerId': self.account_id,
            'name': self.name,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'timestamp': self.timestamp
        }
        return body

    def __repr__(self):
        return '<Bike> {0}: {1} : {2} :{3} :{4} :{5}'.format(self.id, self.account_id, self.latitude,
                                                             self.longitude, self.timestamp, self.name)
