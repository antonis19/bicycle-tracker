from app.models import Account, Bike, TheftReport
from app import db, app
import datetime


def init_db(app):
    app.config.from_pyfile('config.py')
    db.init_app(app)
    db.app = app


def create_tables():
    db.drop_all()
    db.create_all()
    db.session.commit()


def add_accounts():
    admin = Account('admin', '1234')
    guest = Account('guest', 'password')
    db.session.add(admin)
    db.session.add(guest)
    db.session.commit()


def add_bikes():
    db.session.add(Bike(1, 53.2053186, 6.5519019, datetime.datetime.now(), "My black bike"))
    db.session.add(Bike(2, 53.2193264, 6.5575646, datetime.datetime.now(), "Purple Turbo Motor bicycle"))
    db.session.add(Bike(3, 53.2242074, 6.5510306, datetime.datetime.now(), "Mom's bike"))
    db.session.add(Bike(1, 53.2195224, 6.5663996, datetime.datetime.now(), "Super Flying bicycle"))
    db.session.commit()


def seed_database():
    accounts_exist = Account.query.all()
    if not accounts_exist:
        create_tables()
        add_accounts()
        add_bikes()
        add_theft_reports()


def add_theft_reports():
    db.session.add(TheftReport(1, 53.2053186, 6.5519019, datetime.datetime.now()))
    db.session.add(TheftReport(2, 53.2193264, 6.5575646, datetime.datetime.now()))
    db.session.add(TheftReport(3, 53.2183624, 6.5518886, datetime.datetime.now()))
    db.session.add(TheftReport(4, 53.2194414, 6.5436916, datetime.datetime.now()))
    db.session.add(TheftReport(2, 53.2242074, 6.5510306, datetime.datetime.now()))
    db.session.add(TheftReport(3, 53.2263904, 6.5524896, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2369734, 6.5629396, datetime.datetime.now()))
    db.session.add(TheftReport(2, 53.2296264, 6.5396366, datetime.datetime.now()))
    db.session.add(TheftReport(3, 53.2170354, 6.5618036, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2181144, 6.5586496, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2186494, 6.5611426, datetime.datetime.now()))
    db.session.add(TheftReport(3, 53.2195224, 6.5663996, datetime.datetime.now()))

    db.session.add(TheftReport(4, 53.2214744, 6.5705626, datetime.datetime.now()))
    db.session.add(TheftReport(2, 53.2156274, 6.5816776, datetime.datetime.now()))
    db.session.add(TheftReport(3, 53.2288954, 6.5826016, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2371814, 6.5443426, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2294394, 6.5583036, datetime.datetime.now()))
    db.session.add(TheftReport(1, 53.2272934, 6.5516946, datetime.datetime.now()))

    db.session.commit()


def init():
    init_db(app)
    seed_database()
