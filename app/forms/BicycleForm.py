from wtforms import Form, StringField, validators, PasswordField


class BicycleForm(Form):
    bicycle = StringField('Bicycle', [validators.Length(min=4, max=25)])
