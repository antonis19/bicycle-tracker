from flask_login import LoginManager
from app.models.Account import Account
from app import app


def init():
    login_manager = LoginManager()
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        acc = Account.query.get(user_id)
        if not acc:
            return
        acc.is_authenticated = True
        return acc

    # @login_manager.request_loader
    def request_loader(request):
        username = request.form.get('username')
        account = Account.query.filter_by(username=username).first()
        if Account:
            account.is_authenticated = request.form['password'] == account.password
            return account
        else:
            return None
