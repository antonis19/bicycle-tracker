# Bicycle Tracker 

Authors: Elton Antonis, Daan Raatjes, Leonardas Persaud

An application that allows users to track their bikes, and shows a live traffic map of bikes in a city. Users can report bike thefts, and see a heatmap of theft locations.
The application was created as part of the Net Computing course at the University of Groningen. The use cases are somewhat unrealistic since the
goal of the project was mainly to use the following concepts in the application:

- Load Balancing: accomplished using a Star Network.
- Socket Programming: using Python sockets.
- Message Queueing: using RabbitMQ to queue GPS client locations for the traffic map.
- a RESTful API : implemented with Flask to create an interface for reporting bike thefts and obtaining the theft locations heatmap.

[Demo](https://youtu.be/ZXtvEbtvq0A)

## Requirements
This application can be run in a docker container. Therefore, the minimal requirements consist of:
- docker
- docker-compose

The Docker rights need to be configured properly as well. See the [Docker manual](https://docs.docker.com/install/linux/linux-postinstall/) for a guide on how to do this.

Check if Docker is properly installed by running:

`docker info`

and

`docker-compose --version`

If both commands succeed you are ready to install the application.

## Installing the application
First, make sure you have downloaded this repository.

The application can then easily be installed using `docker-compose`. For your convenience a bash script is provided which executes the required commands.

Install the application using the bash script:

`sh ./install-and-start.sh`

This will properly configure the docker containers. Once it's done it will run all services, and your application should be working. Check if it is working by accessing the webinterface on `localhost:5000`.

Once the application is installed, and you only want to start it, you do not have to reinstall it. You can simple run the starting bash script:

`sh ./start.sh`

Note that Docker will run the following services:
- RabbitMQ for message queueing
- A regional server
- The Main server and API server

## User credentials
To login into the webinterface, you can use the following credentials:
> **Username:** admin
>
> **Password:** 1234

## Running the regional servers and clients
To run the regional servers or clients you will need to have Python 3.5 installed.

Installing can then easily be done by running the following command in the root directory of the project:

`pip3 install -r ./requirements.txt` 

Note that you might need to execute this with root privileges.


To run a client or a regional server, you might first have to configure the proper IP settings in their corresponding config files. 
To run a regional server the following command can be used:

`python3 app/regionalserver/regionalServer.py`

To run a client the following command can be used:

`python3 app/client/client.py <bikeId>`

Where `<bikeId>` is the id of the bike you want to simulate. 

Example:

`python3 app/client/client.py 2`

For the bike with id 2.
